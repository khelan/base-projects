package ${package}.service1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(Service1Application.class)
public class Service1Server {
	public static void main(final String[] args) {
		System.setProperty("spring.config.name", "service1-server");
		SpringApplication.run(Service1Server.class, args);
	}
}
