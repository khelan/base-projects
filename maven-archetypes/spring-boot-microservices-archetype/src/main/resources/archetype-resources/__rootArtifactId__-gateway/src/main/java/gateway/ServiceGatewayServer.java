package ${package}.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
@EnableEurekaClient
public class ServiceGatewayServer {
	public static void main(final String[] args) {
		System.setProperty("spring.config.name", "gateway-server");
		SpringApplication.run(ServiceGatewayServer.class, args);
	}
}
