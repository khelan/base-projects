package ${package}.batch;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/batch/simpleBatch-job.xml", "/batch/test-simpleBatch.xml" })
public class SimpleBatchTest {
	@Inject
	private JobLauncherTestUtils jobLauncherTestUtils;

	@Test
	public void simpleBatchTest() throws Exception {
		final JobExecution jobExecution = jobLauncherTestUtils.launchJob();
		Assert.assertEquals(BatchStatus.COMPLETED, jobExecution.getStatus());
	}
}
