# Base Projects

## Maven Archetypes

### How to install Maven Archetypes

```
mvn clean install
```

### How to create a project from an Archetype

```
mvn archetype:generate \
	-DarchetypeGroupId=fr.khelan.maven.archetypes -DarchetypeArtifactId=$ARCHETYPE_ID -DarchetypeVersion=$ARCHETYPE_VERSION \
	-DgroupId=$PROJECT_GROUP -DartifactId=$PROJECT_ARTIFACT -Dversion=$PROJECT_VERSION
```